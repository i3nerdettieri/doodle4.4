package com.mycompanu.mywebapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;

public class CreaEvento extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static CreaEventoUiBinder uiBinder = GWT.create(CreaEventoUiBinder.class);
	private String username,nome,luogo,descrizione;
	private String[] sondaggi;
	private int livSic;
	
	@UiField Button button;
	@UiField TextBox textbox_nome;
	@UiField TextBox textbox_luogo;
	@UiField TextBox textbox_descrizione;
	@UiField Button salva_dati;
	@UiField Button aggiorna;
	@UiField Button modifica_dati;
	@UiField Button sondaggio;
	@UiField Button registra_evento;
	String totale = null;

	interface CreaEventoUiBinder extends UiBinder<Widget, CreaEvento>
	{
	}

	public CreaEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	void setText(boolean general)
	{
		registra_evento.setEnabled(general);
		modifica_dati.setEnabled(general);
		sondaggio.setEnabled(general);
	}
	
	void setDati(String username,int livSic){
		this.username = username;
		this.livSic = livSic;
		setText(false);
		System.out.println("username: " + username);
	}
	
	@UiHandler("button")
	void onButtonClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		GestisciEvento obj = new GestisciEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("salva_dati")
	void onSalva_datiClick(ClickEvent event)
	{
		nome =textbox_nome.getText();
		luogo = textbox_luogo.getText();
		descrizione = textbox_descrizione.getText();
		setText(true);
		sondaggi = new String[] {"25/05/92","30/08/05","07/08/09"};
	}
	
	@UiHandler("registra_evento")
	void onRegistra_eventoClick(ClickEvent event)
	{
	totale = nome +" , "+luogo+" , "+descrizione+" , "+username;
	
	/*
	 * Scomposizione dell array di stringa contenente i dati dei sondaggi
	 * usiamo un identificatore di 'separazione' diversa '-'
	 */
	totale = totale + " - " + sondaggi[0];
	for (int i = 1; i < sondaggi.length; i++) {
        totale = totale + " , " + sondaggi[i];
        System.out.println(sondaggi[i]);
    }
	
		greetingService.inserisciEvento(totale, new  AsyncCallback<String>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("Errore in crea evento, in particolare registra evento");
				
			}

			public void onSuccess(String result)
			{
				System.out.println("Evento creato");
				RootPanel root = RootPanel.get();
				GestisciEvento obj = new GestisciEvento();
				obj.setDati(result, livSic);
				root.clear();
				root.add(obj);
			}
			
		});
		
		
	}


	@UiHandler("sondaggio")
	void onSondaggioClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		Sondaggi obj = new Sondaggi();
		root.clear();
		root.add(obj);
	}


}