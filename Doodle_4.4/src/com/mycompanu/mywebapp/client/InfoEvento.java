package com.mycompanu.mywebapp.client;

import java.io.File;
import java.util.ArrayList;
//import java.util.Scanner;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.AbsolutePanel;

public class InfoEvento extends Composite {

	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static InfoEventoUiBinder uiBinder = GWT.create(InfoEventoUiBinder.class);
	
	private String username;
	private int livSic,idEvento;
	@UiField Button partecipa_eventi;
	@UiField Button gestisci_eventi;
	@UiField Button vedi_commenti;
	@UiField Button sondaggio;
	@UiField Button button_4;
	@UiField TextBox ID_evento;
	@UiField TextBox luogo_evento;
	@UiField TextBox ADMIN;
	@UiField TextBox stato;
	@UiField TextBox partecipanti;
	@UiField TextBox usernameText;
	@UiField TextBox nomeEvento;
	@UiField AbsolutePanel sondaggio_2;
	@UiField Button salva;

	interface InfoEventoUiBinder extends UiBinder<Widget, InfoEvento>
	{
	}

	public InfoEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
		ID_evento.setText("Id evento");
		luogo_evento.setText("luogo");
		stato.setText("stato");
		partecipanti.setText("10000");
	}
	
	void setDati(String username,int livSic,int idEvento){
		this.username = username;
		this.livSic = livSic;
		this.idEvento = idEvento;
		ID_evento.setText(idEvento+"");
		usernameText.setText(username);
		stato.setText("stato");
		
		 if (livSic==0)
	        {
	            gestisci_eventi.setVisible(false);
	            vedi_commenti.setVisible(false);
	            System.out.println("username non registrato");
	            usernameText.setEnabled(true);
	        }
		 if(livSic!=3){
			 ADMIN.setText("username: ");
		 }
		 
		 //askEvento();
		 alternativeAsk();
		 
	}

	/*
	void superSet(String allDati){
		System.out.println(allDati);
		Scanner scanner = new Scanner(allDati);
		Scanner scanD = scanner.useDelimiter("\\s*\\-\\s*");
		
		Scanner scanT;
		String dati = scanD.next();
		scanT = new Scanner(dati);
		Scanner scan = scanT.useDelimiter("\\s*\\,\\s*");
		//**************************************
		
		//***************
		String nome = scanT.next();
		String luogo = scanT.next();
		luogo_evento.setText(luogo);
		String descrizione = scanT.next();

		nomeEvento.setText(nome + " di: " + username);
		scan.close();
		scanT.close();
		
		System.out.println(nome);
		System.out.println(luogo);
		System.out.println(descrizione);
		System.out.println(username);
		//******************
		
		String sondaggiA = scanD.next();
		Scanner scanner2 = new Scanner(sondaggiA);
		Scanner sond = scanner2.useDelimiter("\\s*,\\s*");
		ArrayList<String> tempS = new ArrayList<String>();
		
		while (sond.hasNext()) 
        {
            tempS.add(sond.next());
        }
		
		sond.close();
		scanner.close();
		scanner2.close();
		
		String[] s = new String[tempS.size()];
		
		for (int i =0; i< tempS.size();i++)
        {
			System.out.println(tempS.get(i));
			s[i]=(tempS.get(i));
        }
		for (int i =0; i< tempS.size();i++)
        {
			s[i]=(tempS.get(i));
			System.out.println("s" +i+ " inserisco - " + tempS.get(i) + " - risulta - " + s[i]);
        }
		System.out.println("***************");
		for (int i = 0; i < s.length; i++) {
	        System.out.println(s[i]);
	    }
	}
	
	void askEvento(){

		
		greetingService.prendiEvento(idEvento+"", new  AsyncCallback<String>()
				{
					public void onFailure(Throwable caught)
					{
						System.out.println("Errore in controllo evento");
						
					}

					public void onSuccess(String result)
					{
						System.out.println(result);
						superSet(result);
					}
				});
		
	}
	
	*/
	
	void alternativeAsk(){
		greetingService.getDati(idEvento+"", new  AsyncCallback<String[]>()
				{
					public void onFailure(Throwable caught)
					{
						System.out.println("Errore  getDati");
						
					}

					public void onSuccess(String[] result)
					{
						System.out.println(result);
						superSetAL(result);
					}
				});
	}
	
	void superSetAL(String[] allDati){
		System.out.println(allDati);
		//**************************************
		String[] a = allDati;
		//***************
		String nome = a[0];
		String luogo = a[1];
		luogo_evento.setText(luogo);
		String descrizione = a[2];

		nomeEvento.setText(nome + " di: " + username);
		ArrayList<String> date = new ArrayList<String>();
		
		System.out.println(nome);
		System.out.println(luogo);
		System.out.println(descrizione);
		System.out.println(username);
		//******************
		for(int i=3;i<a.length;i++){
			date.add(a[i]);
		}
		
		
		
		
		for (int i =0; i< date.size();i++)
        {
			System.out.println(date.get(i));
        }
		
		System.out.println("***************");
		for (int i = 0; i < a.length; i++) {
	        System.out.println(a[i]);
	    }
	}
	
	
	@UiHandler("partecipa_eventi")
	void onPartecipa_eventiClick(ClickEvent event) 
	{
		RootPanel root = RootPanel.get();
		PartecipaEvento obj = new PartecipaEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	@UiHandler("gestisci_eventi")
	void onGestisci_eventiClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		GestisciEvento obj = new GestisciEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	@UiHandler("vedi_commenti")
	void onVedi_commentiClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		CommentiEvento obj = new CommentiEvento();
		root.clear();
		root.add(obj);
	}
	@UiHandler("sondaggio")
	void onSondaggioClick(ClickEvent event)
	{
		RootPanel rootPanel = RootPanel.get();
		InfoEvento2 obj = new InfoEvento2();
		rootPanel.clear();
		rootPanel.add(obj);
	}
	@UiHandler("button_4")
	void onButton_4Click(ClickEvent event)
	{
		RootPanel rootPanel = RootPanel.get();
		InfoEvento2 obj = new InfoEvento2();
		rootPanel.clear();
		rootPanel.add(obj);
	}
	@UiHandler("salva")
	void onSalvaClick(ClickEvent event) {
	}
}