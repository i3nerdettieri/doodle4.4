/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompanu.mywebapp.server;


import java.io.File;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.mycompanu.mywebapp.client.Utente;


/**
 *
 * @author Anesario
 */
public class CreaId 
{
    public static int NRandom()
    {
        int crea = (int) (Math.random()*10000);
        return crea;
    }
    public static int  creaID(String from,int type)
    {
        int id = NRandom();
        DB db = DBMaker.newFileDB(new File("doodle")).closeOnJvmShutdown().make();
        
        /*
         * switch-case per creare l albero di ricerca opportuno
         */
        switch (type) {
            case 1:  ConcurrentNavigableMap<Integer,Utente> mapId = db.getTreeMap(from);
		            while(mapId.containsKey(id))
			        {
			            id = NRandom();
			        }
		            
            		 break;
            case 2:  ConcurrentNavigableMap<String,String> mapIdEv = db.getTreeMap(from);
		            while(mapIdEv.containsKey(""+id))
			        {
			            id = NRandom();
			        }
            		 break;
            case 3:  ConcurrentNavigableMap<Integer,String> mapIdUt = db.getTreeMap(from);
		            while(mapIdUt.containsKey(id))
			        {
			            id = NRandom();
			        }
            		 break;
            default: System.out.println("qualcosa e' andato storto");
            		 break;
        }
        db.close();
        
        return id;
    }
}
    
    /*
     * 
     * 	credo non utilizzato
     * 
     
     
    public static int superCreaID(String from,String where,String from2,String where2)
    {
        String id = Integer.toString(NRandom());
        while(Connect.searchString(from,where,id)&&(Connect.searchString(where2, from2, id))){
            id= Integer.toString(NRandom());
        }
        int iD = Integer.parseInt(id);
        return iD;
    }
    */
